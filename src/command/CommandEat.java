package command;

import interfaces.Edible;
import items.Item;
import textadventure.World;

public class CommandEat extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"eat", "consume", "chew"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// Handle errors
		if (params.length != 1) {
			World.print("What do you want to eat?\n\n");
			return;
		}
		String itemName = params[0];
		if (!world.getPlayer().hasItem(itemName)) {
			World.print("You don't have the " + itemName + ".\n\n");
			return;
		}
		else {
			Item item = world.getPlayer().getItem(itemName);
			if (item instanceof Edible)
				((Edible)item).doEat();
			else
				World.print("That's plainly inedible!\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
		return "[item]";
	}
}
