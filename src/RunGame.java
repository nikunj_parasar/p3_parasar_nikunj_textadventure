/*
 * P3 Nikunj Parasar Time taken: 3 hours 2/26/2020 
 * 
 * 
 * I learned about hashmaps in this lab, and also 
 * was able to explore the features and complexity of 
 * the starter interfaces and classes. I got to better 
 * understand the difference between interfaces and
 * abstract classes and the applicable uses of each 
 * one. The lab also demonstrated how abstract classes 
 * can have a method with no body and also not implement 
 * methods from interfaces. i got a better understanding
 * of how Interfaces and abstract methods are like ideas 
 * and you can not initialize an object of an abstract class
 * since there has to be some sort of implementation of that idea. 
 * you can implement as many interfaces as you want, but you can only 
 * extend one class (abstract or not).

 * 
 * */




import textadventure.DemoWorld;

public class RunGame {
	
	public static void main(String[] args) {
		new DemoWorld().play();
	}	
}