package textadventure;

public class OutsideRoom extends Room{

	public OutsideRoom(String name, String description, boolean isLocked, World world) {
		super(name, description, isLocked, world);
		// TODO Auto-generated constructor stub
	}
	
	public void doEnter() {
		World.print("your carpool arrives\n");
		if(!getWorld().getPlayer().isHasBrushedTeeth()) {
			World.print("Your carpool mates notice that you haven't brushed your teeth.  How embarrassing!  Game over!\n");
			System.exit(0);
		}
		if(getWorld().getPlayer().getHealth()!= 2) {
			World.print("Your stomach churns as you realize you forgot to toast the bread.  You get sick in on the way to school.  How embarrassing!  Game over! \n");
			System.exit(0);
		}
		if(!getWorld().getPlayer().isWearingClothes()) {
			World.print("You arent wearing any clothes! You get embarassed. game over\n");
			System.exit(0);
		}
		
		World.print("Have a great day at school. you win");
	}
	
}
