package items;

import textadventure.World;

public class Toothbrush extends Item{

	public Toothbrush(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUse() {
		// TODO Auto-generated method stub
		World.print("You brushed your teeth");
		getWorld().getPlayer().setHasBrushedTeeth(true);
	}
	
}
