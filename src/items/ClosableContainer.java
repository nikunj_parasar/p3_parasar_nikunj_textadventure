package items;

import interfaces.Closeable;
import textadventure.World;

public class ClosableContainer extends Container implements Closeable{

	private boolean isOpen;
	
	public ClosableContainer(World world, String name, int weight, boolean takeable, String description, boolean isOpen) {
		super(world, name, weight, takeable, description);
		this.isOpen = isOpen;
	}

	@Override
	public boolean isOpen() {
		return isOpen;
	}

	@Override
	public void doOpen() {
		this.isOpen = Closeable.OPEN;
		World.print("Opened\n");
	}

	@Override
	public void doClose() {
		this.isOpen = Closeable.CLOSED;
		World.print("Closed\n");
	}

	@Override
	public void doExamine() {
		if(isOpen) {
			World.print(getDescription() + " Inside the " + getName() + " you see " + getItemString() + ".\n\n");	
		}
		else {
			World.print("The " + getName() + " is closed\n");
		}
	}
	
	
	@Override 
	public Item doTake(Item item) {
		if(!isOpen) {
			World.print("you can't take anything, the " + getName() + " is closed\n");
			return null;
		}
		else {
			return super.doTake(item);
		}
	}
	
	@Override
	public Item doPut(Item item, Container source) {
		if(!isOpen) {
			World.print("You can't put anything, the " + getName() + " is closed\n");
			return null;
		}
		else {
			return super.doPut(item, source);
		}
	}
}
