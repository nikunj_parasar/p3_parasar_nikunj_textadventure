package items;

import interfaces.Edible;
import textadventure.World;

public class Food extends Item implements Edible{
	public Food(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}

	public void doEat() {
		World.print("you eat the " + getName() + " and feel stronger!\n");
		getWorld().getPlayer().removeItem(this);
		getWorld().getPlayer().setHealth(getWorld().getPlayer().getHealth() + getWeight());
		getWorld().getPlayer().setHasBrushedTeeth(false);
	}

	@Override
	public void doUse() {
		// TODO Auto-generated method stub
		doEat();
	}
}
