package items;

import textadventure.DemoWorld;
import textadventure.World;

public class Keys extends Item{

	public Keys(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doUse() {
		if(getWorld().getPlayer().getCurrentRoom().getName().equals(DemoWorld.HALLWAY)) {
			getWorld().getRoom(DemoWorld.BATHROOM).doUnlock();
			World.print("You unlock the bathroom \n");
		}
		else if(getWorld().getPlayer().getCurrentRoom().getName().equals(DemoWorld.KITCHEN)) {
			getWorld().getRoom(DemoWorld.OUTSIDE).doUnlock();
			World.print("You unlock the door leading outside\n");
		}
		else {
			World.print("The " + getName() + "doesnt fit anything here");
		}
	}
	
}
